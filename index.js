/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:


    function getUserInformations(){
        let fullName = prompt("Enter your Full Name:");
        let age = prompt("Enter your Full age:");
        let location = prompt("Enter your location:");

        console.log("Hello," + fullName);
        console.log("You are " + age + " years old");
        console.log("You live in " + location);
    }
    getUserInformations();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
    const topFiveFavoriteBands = ["Paramore", "Puddle of Mudd", "Franco", "Eraserheads", "Urbandub"]; 
    function showTopFavoriteBands(){
        for (i=0;i<topFiveFavoriteBands.length;i++){
            console.log( i+1 +". "+topFiveFavoriteBands[i]);
        }
    }
    showTopFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
    const topFiveFavoriteMovies = [["WarmBodies", "97%"], ["Avengers Endgame", "94%"], ["Spider-Man: No Way Home","93%"],["	Harry Potter","81%"], ["The Hunger Games", "69%"]]; 
    function showTopFavoriteMovies(){
        for (i=0;i<topFiveFavoriteMovies.length;i++){
            console.log( i+1 +". "+topFiveFavoriteMovies[i][0]);
            console.log("Rotten Tomatoes Rating:" + topFiveFavoriteMovies[i][1]);
        }
    }
    showTopFavoriteMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
    

};
printFriends();

